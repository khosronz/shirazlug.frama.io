---
title: "جلسه ۱۳۶"
description: "میزگرد با موضوع جمع‌آوری اطلاعات در اوبونتو"
date: "1396-11-30"
author: "مریم بهزادی"
categories:
    - "sessions"
summaryImage: "/img/posters/poster136.jpg"
---
[![poster136](../../img/posters/poster136.jpg)](../../img/poster136.jpg)